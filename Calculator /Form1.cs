﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        double ResultValue = 0;
        string OperationPerformed = "";
        bool isOperationPerformed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            if(ScreenBox.Text == "0" || isOperationPerformed)
            {
                ScreenBox.Clear(); //da se rijesim defaultne 0
            }
            isOperationPerformed = false;
            Button button = (Button)sender;

            if(button.Text == ".")
            {
                if(!ScreenBox.Text.Contains("."))
                    ScreenBox.Text = ScreenBox.Text + button.Text; //zaustavljanje dublih tocaka
            }
            else
            ScreenBox.Text = ScreenBox.Text + button.Text; //kod da svaki click na gumb ispise taj broj ili znak na kalkulator
        }

        private void operator_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (ResultValue != 0)
            {
                equals.PerformClick();
                OperationPerformed = button.Text;
                label_Current_Operation.Text = ResultValue + " " + OperationPerformed;
                isOperationPerformed = true;
            }
            else
            {
                OperationPerformed = button.Text;
                ResultValue = double.Parse(ScreenBox.Text);
                label_Current_Operation.Text = ResultValue + " " + OperationPerformed;
                isOperationPerformed = true;
            }
        }

        private void Result_Click(object sender, EventArgs e)
        {
            switch (OperationPerformed)
            {
                //ovisiti ce o provedenoj operaciji
                case "+":
                    ScreenBox.Text = (ResultValue + double.Parse(ScreenBox.Text)).ToString();
                    break;
                case "-":
                    ScreenBox.Text = (ResultValue - double.Parse(ScreenBox.Text)).ToString();
                    break;
                case "*":
                    ScreenBox.Text = (ResultValue * double.Parse(ScreenBox.Text)).ToString();
                    break;
                case "/":
                    ScreenBox.Text = (ResultValue / double.Parse(ScreenBox.Text)).ToString();
                    break;
                case "exp":
                    double pow = double.Parse(ScreenBox.Text);
                    double ans = Math.Exp(pow * Math.Log(ResultValue * 4));
                    ScreenBox.Text = ans.ToString();
                    break;
                default:
                    break;
            }
            ResultValue = double.Parse(ScreenBox.Text);
            label_Current_Operation.Text = "";
        }
        private void C_Click(object sender, EventArgs e)
        {
            ScreenBox.Text = "0"; //vraca sve na 0
            ResultValue = 0;

        }

        private void CE_Click(object sender, EventArgs e)
        {
            ScreenBox.Text = "0"; //clear entry samo vraca entry na 0
        }

        private void log_Click(object sender, EventArgs e)
        {
            if (ResultValue == 0)
            {
                label_Current_Operation.Text = " " + OperationPerformed + "log" + " " + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Log10(double.Parse(ScreenBox.Text))).ToString();
            }
            else
            {
                label_Current_Operation.Text = ResultValue + " " + OperationPerformed + " " + "log" + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Log10(double.Parse(ScreenBox.Text))).ToString();
            }
        }

        private void sin_Click(object sender, EventArgs e)
        {
            if (ResultValue == 0)
            {
                label_Current_Operation.Text = " " + OperationPerformed + "sin" + " " + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Sin(double.Parse(ScreenBox.Text))).ToString();
            }
            else
            {
                label_Current_Operation.Text = ResultValue + " " + OperationPerformed + " " + "sin" + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Sin(double.Parse(ScreenBox.Text))).ToString();
            }
        }

        private void cos_Click(object sender, EventArgs e)
        {
            if (ResultValue == 0)
            {
                label_Current_Operation.Text = " " + OperationPerformed + "cos" + " " + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Cos(double.Parse(ScreenBox.Text))).ToString();
            }
            else
            {
                label_Current_Operation.Text = ResultValue + " " + OperationPerformed + " " + "cos" + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Cos(double.Parse(ScreenBox.Text))).ToString();
            }
        }

        private void tan_Click(object sender, EventArgs e)
        {
            if (ResultValue == 0)
            {
                label_Current_Operation.Text = " " + OperationPerformed + "tan" + " " + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Tan(double.Parse(ScreenBox.Text))).ToString();
            }
            else
            {
                label_Current_Operation.Text = ResultValue + " " + OperationPerformed + " " + "tan" + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Tan(double.Parse(ScreenBox.Text))).ToString();
            }
        }

        private void sqrt_Click(object sender, EventArgs e)
        {
            if (ResultValue == 0)
            {
                label_Current_Operation.Text = " " + OperationPerformed + "sqrt" + " " + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Sqrt(double.Parse(ScreenBox.Text))).ToString();
            }
            else
            {
                label_Current_Operation.Text = ResultValue + " " + OperationPerformed + " " + "sqrt" + "(" + (ScreenBox.Text) + ")";
                ScreenBox.Text = (Math.Sqrt(double.Parse(ScreenBox.Text))).ToString();
            }
        }

        private void backspace_Click(object sender, EventArgs e)
        {
            if(ScreenBox.Text.Length > 0)
            {
                ScreenBox.Text = ScreenBox.Text.Remove(ScreenBox.Text.Length - 1, 1);
            }
        }

        private void switchs_Click(object sender, EventArgs e)
        {
            ResultValue = double.Parse(ScreenBox.Text);
            ResultValue = ResultValue * -1;
            ScreenBox.Text = ResultValue.ToString();
        }
    }
}
